# Tenpo Technical Test
## Pasos a seguir
- Es necesario tener instalado [docker](https://docs.docker.com/desktop/)
- Una vez clonado el repositorio a través del terminal ubicarse en el directorio ./test/TenpoTest y ejecutar el comando `docker compose up`
- Cuando en el terminal aparesca el mensaje 
  > Started TenpoTestApplication in 16.594 seconds (JVM running for 17.99)
  
 ingresar a http://localhost:80/swagger-ui.html desde donde podran probar el servicio desarrollado
