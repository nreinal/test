package com.tenpo.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthRequest {
	@NotNull(message = "Nombre de usuario no puede ser nulo")	
	@NotBlank(message = "Nombre de usuario no puede ser vacio")
	private String username;
	@NotNull(message = "Password no puede ser nulo")
	@NotBlank(message = "Password no puede ser vacio")
	private String password;
}
