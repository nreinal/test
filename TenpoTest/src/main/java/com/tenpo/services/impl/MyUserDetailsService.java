package com.tenpo.services.impl;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.tenpo.entities.UserEntity;
import com.tenpo.repositories.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MyUserDetailsService implements UserDetailsService{
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO DEBO VALIDAR EN BD QUE EL USUARIO QUE ME ENTREGAN ES CORRECTO
		Optional<UserEntity> userFound = userRepository.findByUsername(username);		
		if(userFound.isPresent()) {
			log.info("usuario existe");
			UserEntity user = userFound.get();
			return new User(user.getUsername(), user.getPassword(), new ArrayList<>());	        
		}
		else {
			log.info("Usuario no registrado");
		}
		return null;		
	}
	
}
