package com.tenpo.services.impl;

import org.springframework.stereotype.Service;

import com.tenpo.services.SumaService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SumaServiceImpl implements SumaService{
	
	
	@Override
	public float suma(float firstNumber, float secondNumber) {
		log.info("primer numero: {}", firstNumber);
		log.info("segundo numero: {}", secondNumber);
		return firstNumber + secondNumber;
	}
}
