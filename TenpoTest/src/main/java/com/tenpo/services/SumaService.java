package com.tenpo.services;

public interface SumaService{
	
	public float suma(float firstNumber, float secondNumber);
}
