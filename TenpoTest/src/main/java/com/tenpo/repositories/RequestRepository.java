package com.tenpo.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.tenpo.entities.RequestEntity;

@Repository
public interface RequestRepository extends CrudRepository<RequestEntity, Long>, PagingAndSortingRepository<RequestEntity, Long>{
	
	Page<RequestEntity> findAll(Pageable pageable);

}
