package com.tenpo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tenpo.entities.TokenBanEntity;

@Repository
public interface TokenBanRepository extends CrudRepository<TokenBanEntity, Long>{

}
