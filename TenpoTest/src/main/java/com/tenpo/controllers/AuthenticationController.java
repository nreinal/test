package com.tenpo.controllers;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tenpo.dto.AuthRequest;
import com.tenpo.dto.AuthResponse;
import com.tenpo.models.TokenBanModel;
import com.tenpo.models.UserModel;
import com.tenpo.services.impl.JwtServiceImpl;
import com.tenpo.services.impl.MyUserDetailsService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/tenpoTest")
public class AuthenticationController {
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private MyUserDetailsService userDetailsService;
	
	@Autowired
	private JwtServiceImpl jwtService;
	
	@Autowired
	private UserModel userModel;
	
	@Autowired
	private TokenBanModel tokenBanModel;
	
	@PostMapping(value = "/auth")
	public ResponseEntity<?> createAuthenticationToken(@Valid @RequestBody AuthRequest request) throws Exception{
		try {
			authenticationManager.authenticate(
						new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));								
		}
		catch(BadCredentialsException e) {			
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Credenciales incorrectas");		
		}		
		final UserDetails userDetails = userDetailsService.loadUserByUsername(request.getUsername());
		final String jwt = jwtService.generateToken(userDetails);		
		return ResponseEntity.ok(new AuthResponse(jwt));
	}
	
	@PostMapping(value= "/singup")
	public ResponseEntity<?> createUser(@Valid @RequestBody AuthRequest request){
		//agerga un nuevo usuario en la BD
		if(userModel.creaNuevoUsuario(request)) {
			return ResponseEntity.status(HttpStatus.CREATED).body("Nuevo usuario creado");
		}
		else {
			return ResponseEntity.status(HttpStatus.CONFLICT).body("El nombre de usuario ya existe");
		}
		
		
	}
	
	@GetMapping(value= "/logout")
	public ResponseEntity<?> logout(@RequestHeader String auth){
		try {
			String token = auth.substring(7);
			String userName = jwtService.extractUserName(token);
			log.info("user logout: {}, token: {}", userName, token);
			Long userId = userModel.getId(userName);				
			tokenBanModel.addTokenBan(userId, token);		
			return ResponseEntity.status(HttpStatus.OK).body("logout "+userName+" ok");
		}
		catch(Exception e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Credenciales incorrectas");
		}
		
	}

}
