package com.tenpo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.tenpo.models.RequestModel;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/tenpoTest")
public class LogStoryController {
	
	@Autowired
	private RequestModel requestModel;
	
	@GetMapping(value = "/getRequestHistory")
	public HttpEntity<?> getRequestHistory(
			@RequestHeader("Authorization") String auth,
			@RequestParam("page") int page, 
			@RequestParam("size") int size){
		//TODO
		return new HttpEntity<String>(new Gson().toJson(requestModel.getAllLogHistory(page, size)));
	}

}
