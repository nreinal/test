package com.tenpo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tenpo.dto.SumaRequest;
import com.tenpo.services.SumaService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/tenpoTest")
public class SumaController {
	
	@Autowired
	private SumaService sumaService;
	
	@PostMapping(value = "/suma")
	public HttpEntity<?> sumNumbers(
			@RequestHeader("Authorization") String auth, 
			@RequestBody SumaRequest request) {
		float result = sumaService.suma(request.getFirstNumber(), request.getSecondNumber());
		log.info("resultado: {}", result);
		return new HttpEntity<String>(Float.toString(result));
	}
	
	
}

	

