package com.tenpo.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.tenpo.models.RequestModel;
import com.tenpo.util.HttpServletReqUtil;
import com.tenpo.util.MyHttpServletRequestWrapper;

import lombok.extern.slf4j.Slf4j;

@Component
@Order(1)
@Slf4j
public class TransactionFilter implements Filter {

	@Autowired
	private HttpServletReqUtil reqUtil;
	
	@Autowired
	private RequestModel requestModel;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		final MyHttpServletRequestWrapper wrappedRequest = new MyHttpServletRequestWrapper(
				(HttpServletRequest) request);
		String payload = reqUtil.getPayLoad(wrappedRequest);
		String token = reqUtil.getToken(wrappedRequest);
		String url = reqUtil.getURL(wrappedRequest);
		String params = reqUtil.getRequestParams(wrappedRequest);
		if((!payload.isEmpty() || wrappedRequest.getMethod().toUpperCase().equals("GET")) && url.contains("/tenpoTest/")) {
			log.info("Inside Servlet Filter");
			log.info("Request Params: {}", params);
			log.info("Request Payload: {}", payload);
			log.info("Request token: {}", token);
			log.info("Request url: {}", url);
			log.info("contiene tenpo:{} ",url.contains("/tenpoTest/"));
			log.info("Exiting Servlet Filter");
			requestModel.saveRequest(payload, token, url, params);
		}		
		chain.doFilter(wrappedRequest, response);
	}
}