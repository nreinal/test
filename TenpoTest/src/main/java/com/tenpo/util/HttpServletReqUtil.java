package com.tenpo.util;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

@Service
public class HttpServletReqUtil {
	
	public String getToken(HttpServletRequest request) {
		String authorization = request.getHeader("Authorization");		
		if(authorization != null) {
			return authorization.substring(7);
		}
		return "";		
	}
	
	public String getURL(HttpServletRequest request) {
		return request.getRequestURL().toString();
	}
	
	public String getRequestParams(HttpServletRequest request) {
		final StringBuilder params = new StringBuilder();

		Enumeration<String> parameterNames = request.getParameterNames();

		for (; parameterNames.hasMoreElements();) {
			String paramName = parameterNames.nextElement();
			String paramValue = request.getParameter(paramName);
			if ("password".equalsIgnoreCase(paramName) || "pwd".equalsIgnoreCase(paramName)) {
				paramValue = "*****";
			}
			params.append(paramName).append(": ").append(paramValue).append(System.lineSeparator());
		}
		return params.toString();

	}
	

	public String getPayLoad(HttpServletRequest request) {

		final String method = request.getMethod().toUpperCase();

		if ("POST".equals(method)) {
			return extractPostRequestBody(request);
		}
		return "";
	}	

	private static String extractPostRequestBody(HttpServletRequest request) {
		if ("POST".equalsIgnoreCase(request.getMethod())) {
			Scanner s = null;
			try {				
				s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return s.hasNext() ? s.next() : "";
		}
		return "";
	}
}