package com.tenpo.models;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tenpo.dto.AuthRequest;
import com.tenpo.entities.UserEntity;
import com.tenpo.repositories.UserRepository;

@Component
public class UserModel {
	
	@Autowired
	private UserRepository userRepository;
	
	@Transactional
	public boolean creaNuevoUsuario(AuthRequest usuario) {		
		if(!userRepository.findByUsername(usuario.getUsername()).isPresent()) {
			UserEntity nuevoUsuario = new UserEntity(usuario.getUsername(), usuario.getPassword());
			userRepository.save(nuevoUsuario);
			return true;
		}
		else {
			return false;
		}
		
	}
	
	public boolean validaUsuario(AuthRequest usuario) {
		Optional<UserEntity> userFound = userRepository.findByUsername(usuario.getUsername());
		if(userFound.isPresent()) {
			UserEntity user = userFound.get();
			if(user.getPassword().equals(usuario.getPassword())) {
				return true;
			}
		}
		return false;
	}
	
	public long getId(String username) {
		Optional<UserEntity> userFound = userRepository.findByUsername(username);
		if(userFound.isPresent()) {
			UserEntity user = userFound.get();
			return user.getId();
		}
		return 0;
	}
}
