package com.tenpo.models;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.tenpo.entities.RequestEntity;
import com.tenpo.repositories.RequestRepository;

@Component
public class RequestModel {
	
	@Autowired
	private RequestRepository requestRepository;
	
	public void saveRequest(String body, String token, String url, String params) {
		RequestEntity request = new RequestEntity();		
		request.setRequestBody(body.replace("\n", ""));
		request.setToken(token);
		request.setUrl(url);		
		request.setParams(String.join(",", params.split("\n")));
		request.setTimestamp(new Timestamp(System.currentTimeMillis()));		
		requestRepository.save(request);
	}
	
	public List<RequestEntity>  getAllLogHistory(int page, int size) {
		Pageable pageRequest = PageRequest.of(page, size);
		return requestRepository.findAll(pageRequest).getContent();
	}
}
