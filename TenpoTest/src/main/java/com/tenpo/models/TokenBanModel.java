package com.tenpo.models;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tenpo.entities.TokenBanEntity;
import com.tenpo.repositories.TokenBanRepository;

@Component
public class TokenBanModel {
	
	@Autowired
	private TokenBanRepository tokenBanRepository;

	public void addTokenBan(Long userId, String token) {
		TokenBanEntity tokenBan = new TokenBanEntity(userId,token);		
		tokenBanRepository.save(tokenBan);
	}
	
	public boolean isBanToken(Long userId, String token) {
		Optional<TokenBanEntity> found =tokenBanRepository.findById(userId);
		if(found.isPresent()) {
			if(found.get().getToken().equals(token)) {
				return true;
			}
		}
		return false;
	}
}
