package com.tenpo.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "token_ban")
public class TokenBanEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	private String token;
}
